package com.example.gilberth.plazamendoza.pm.faci.sigadecano;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Neksze on 02/10/2015.
 */
public class Person {

    @SerializedName("nombre")
    private String nombre;

    @SerializedName("apellido")
    private String apellido;

    @SerializedName("cargo")
    private String cargo;

    @SerializedName("nombramiento")
    private String nombramiento;

    @SerializedName("foto")
    private String foto;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTitulo() {
        return cargo;
    }

    public void setTitulo(String titulo) {
        this.cargo = cargo;
    }

    public String getMateria() {
        return nombramiento;
    }

    public void setMateria(String materia) {
        this.nombramiento = nombramiento;
    }

    public String getImagen() {
        return foto;
    }

    public void setImagen(String imagen) {
        this.foto = foto;
    }
}
